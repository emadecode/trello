# Instapro Company Test

The test task for Instapro Company used React-js and TypeScript.

## Url

https://trello-emadecode.vercel.app/

## Sections

### [Available Scripts](#available-scripts-1)

### [Documentation](#documentation-1)

&nbsp;
&nbsp;

---

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Runs the tests.

&nbsp;
&nbsp;

---

# Documentation

&nbsp;

### First of all,

# Yaaaay! I did it.

&nbsp;

> I know some of the things I did for such a small app would be over-engineering, but as you can imagine, I tried to develop this app with the idea of enterprise applications.

&nbsp;

## Why I have used React Context?

Usually, I will use redux. That’s because we have a cleaner data flow (flux) and also we can track state change more easily with that flow. And it is easier to test. But as you requested, I didn't use it for this project.

## What was my approach to testing?

I’ve used react testing library for unit and integration tests. I also wanted to use cypress for E2E testing. but I wasn't able due to limitations of time.
You can find a custom render function for react testing library on “Core” directory. The reason I created a custom render method was to add required providers for a component that is going to be tested. In this way, if we add a new provider to our top-level component in the future, we can easily change the custom render method and all tests would be edited.

## What was my approach to styling?

I’ve used module CSS. I usually use styled components for styling and theming. But as I mentioned before, I wasn't allowed to use any third-party library.

## Storybook?

The storybook library gives you the ability to see each component separately in an isolation mode. And you can also test different use cases of the components. If I had more time, I would set up a storybook for generic components.

## And last but not least, A CI/CD Tool:

I’ve used Gitlab.com and Vercel.com to implement continuous integration and continuous development. So u can see an online version of the app and we can also make changes on part of the app in the future if you want.
