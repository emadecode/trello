import React, {useCallback} from 'react';

const useLocalstorage = () => {
  const getItem = useCallback(
    <T extends unknown>(key: string, shoudParse: boolean = false): T | null => {
      const item = localStorage.getItem(key);
      if (item) {
        return shoudParse ? (JSON.parse(item) as T) : (item as T);
      }
      return null;
    },
    [],
  );

  const setItem = useCallback(
    <T extends unknown>(
      key: string,
      value: T,
      shoudConvert: boolean = false,
    ): void => {
      shoudConvert
        ? localStorage.setItem(key, JSON.stringify(value))
        : localStorage.setItem(key, value as string);
    },
    [],
  );

  return {
    getItem,
    setItem,
  };
};

export default useLocalstorage;
