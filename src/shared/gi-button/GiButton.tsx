import React, {FC} from 'react';
import styles from './GiButton.module.scss';

interface IGiButtonProps {
  title: string;
  fullwidth?: boolean;
  isGradient?: boolean;
  variant: 'Primary' | 'Secondary';
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

const GiButton: FC<IGiButtonProps> = ({
  title,
  fullwidth,
  isGradient,
  variant,
  onClick,
  ...rest
}) => {
  return (
    <button
      className={`${styles.giButton} ${
        fullwidth && styles.giButton_isFullwidth
      } ${variant === 'Secondary' && styles.giButton_isSecondary} ${
        isGradient && styles.giButton_isGradient
      } `}
      onClick={onClick}
      {...rest}
    >
      {title}
    </button>
  );
};

export default GiButton;
