import React, {FC} from 'react';
import ReactDOM from 'react-dom';
import GiButton from '../gi-button/GiButton';
import styles from './GiDialog.module.scss';

interface IGiDialogProps {
  visible: boolean;
  content: React.ReactElement;
  onClose?: () => void;
  onSubmit?: () => void;
}
const GiDialog: FC<IGiDialogProps> = ({
  visible,
  onClose,
  onSubmit,
  content,
  ...rest
}) =>
  visible
    ? ReactDOM.createPortal(
        <div className={styles.giDialog} {...rest}>
          <div className={styles.giDialogPop} role="dialog" aria-modal="true">
            <div className={styles.giDialogPop__body}>{content}</div>
            <div className={styles.giDialogPop__footer}>
              {onClose && (
                <GiButton
                  title="Cancel"
                  variant="Secondary"
                  onClick={onClose}
                />
              )}
              {onSubmit && (
                <GiButton title="Submit" variant="Primary" onClick={onSubmit} />
              )}
            </div>
          </div>
          <div className={styles.giDialogOverlay}></div>
        </div>,
        document.body,
      )
    : null;

export default GiDialog;
