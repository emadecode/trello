export interface IColumnItemModel {
  id: string;
  title: string;
}
