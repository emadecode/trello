import React, {FC, useContext, useState} from 'react';
import styles from './ColumnItem.module.scss';
import {AppContext} from '../../contexts/app-context';
import {IColumnItemModel} from './ColumnItem.models';
import GiButton from '../../shared/gi-button/GiButton';
import CardItem from '../card-item/CardItem';
import AddCardForm from '../add-card-form/AddCardForm';

interface IColumnItemProps {
  data: IColumnItemModel;
}
const ColumnItem: FC<IColumnItemProps> = ({data}) => {
  const {columns, setColumnsList, cards, setCardsList} = useContext(AppContext);
  const [isOnEditMode, setIsOnEditMode] = useState<boolean>(false);
  const [title, setTitle] = useState<string>(data.title);

  const toggleEditMode = () => {
    setIsOnEditMode(!isOnEditMode);
  };
  const editColumnItem = () => {
    const columnsList = columns.map(item =>
      item.id === data.id ? {...item, title} : item,
    );
    setColumnsList(columnsList);
    setIsOnEditMode(false);
  };
  const deleteColumnItem = () => {
    const columnsList = columns.filter(item => item.id !== data.id);
    const cardList = cards.filter(item => item.columnId !== data.id);
    setCardsList(cardList);
    setColumnsList(columnsList);
    setIsOnEditMode(false);
  };

  const renderCards = () => {
    return cards
      .filter(item => item.columnId === data.id)
      .map(item => <CardItem data={item} key={item.id} />);
  };

  return (
    <div className={styles.columnItem} data-testid="column-item">
      <div className={styles.columnItem__header}>
        {isOnEditMode ? (
          <input
            value={title}
            onChange={event => setTitle(event.target.value)}
            data-testid="title-input"
          />
        ) : (
          <h3 className={styles.columnItem__title} title={data.title}>
            {data.title}
          </h3>
        )}

        {isOnEditMode ? (
          <GiButton title="Submit" variant="Primary" onClick={editColumnItem} />
        ) : (
          <GiButton
            title="Edit"
            variant="Primary"
            onClick={toggleEditMode}
            data-testid="column-edit-button"
          />
        )}
      </div>
      <div className={styles.columnItem__body}>
        <AddCardForm columnId={data.id} />
        {renderCards()}
      </div>
      <div className={styles.columnItem__footer}>
        <GiButton
          title="Delete"
          variant="Secondary"
          fullwidth={true}
          onClick={deleteColumnItem}
          data-testid="column-delete-button"
        />
      </div>
    </div>
  );
};

export default ColumnItem;
