import React from 'react';
import {fireEvent, screen} from '@testing-library/react';
import ColumnItem from './ColumnItem';
import {render} from '../../@core/testProviders';

const columnItemData = {id: 'id-1', title: 'Test Column'};

describe('Test ColumnItem Components', () => {
  it('should load edit button', () => {
    render(<ColumnItem data={columnItemData} />);
    const editButtonEl: HTMLElement = screen.getByTestId('column-edit-button');
    expect(editButtonEl).toBeInTheDocument();
  });
  it('should show title edit mode when click on the edit button', () => {
    render(<ColumnItem data={columnItemData} />);
    const editButtonEl: HTMLElement = screen.getByTestId('column-edit-button');
    fireEvent.click(editButtonEl);
    const titleInputEl: HTMLElement = screen.getByTestId('title-input');
    expect(titleInputEl).toBeInTheDocument();
  });
  it('should load delete button', () => {
    render(<ColumnItem data={columnItemData} />);
    const deleteButtonEl: HTMLElement = screen.getByTestId(
      'column-delete-button',
    );
    expect(deleteButtonEl).toBeInTheDocument();
  });
  it('should load the add card button', () => {
    render(<ColumnItem data={columnItemData} />);
    const addCartButtonEl: HTMLElement = screen.getByTestId('add-card-button');
    expect(addCartButtonEl).toBeInTheDocument();
  });
  it('should load add card inputs when click on the add card button', () => {
    render(<ColumnItem data={columnItemData} />);
    const addCartButtonEl: HTMLElement = screen.getByTestId('add-card-button');
    fireEvent.click(addCartButtonEl);
    const addCardsInputsEl: HTMLElement = screen.getByTestId('add-card-inputs');
    expect(addCardsInputsEl).toBeInTheDocument();
  });
});
