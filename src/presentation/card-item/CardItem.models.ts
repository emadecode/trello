export interface ICardItemModel {
  id: string;
  title: string;
  description: string;
  columnId: string;
}
