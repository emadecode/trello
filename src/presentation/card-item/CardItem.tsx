import React, {FC, useContext, useState} from 'react';
import styles from './CardItem.module.scss';
import {ICardItemModel} from './CardItem.models';
import GiButton from '../../shared/gi-button/GiButton';
import {AppContext} from '../../contexts/app-context';
import GiDialog from '../../shared/gi-dialog/GiDialog';
import {IColumnItemModel} from './../column-item/ColumnItem.models';

interface ICardItemProps {
  data: ICardItemModel;
}
const CardItem: FC<ICardItemProps> = ({data}) => {
  const {setCardsList, cards, columns} = useContext(AppContext);
  const [isOnEditMode, setIsOnEditMode] = useState<boolean>(false);
  const [title, setTitle] = useState<string>(data.title);
  const [description, setDescription] = useState<string>(data.description);
  const [isDialogVisible, setIsDialogVisible] = useState<boolean>(false);

  const toggleEditMode = () => {
    setIsOnEditMode(!isOnEditMode);
  };
  const editCardItem = () => {
    const cardsList = cards.map(item =>
      item.id === data.id ? {...item, title, description} : item,
    );
    setCardsList(cardsList);
    setIsOnEditMode(false);
  };

  const deleteCardItem = () => {
    const cardsList = cards.filter(item => item.id !== data.id);
    setCardsList(cardsList);
    setIsOnEditMode(false);
  };

  const renderColumns = () => {
    return (
      <div className={styles.selectColumn}>
        {columns.map(item => (
          <h4
            className={styles.selectColumn__item}
            key={item.id}
            onClick={() => changeCoLumn(item)}
          >
            {item.title}
          </h4>
        ))}
      </div>
    );
  };

  const changeCoLumn = (column: IColumnItemModel) => {
    const cardList = cards.map(item =>
      item.id === data.id ? {...item, columnId: column.id} : item,
    );

    setCardsList(cardList);
    setIsDialogVisible(false);
  };

  return (
    <div className={styles.cartItem}>
      <div className={styles.cartItem__header}>
        <div>
          <span className={styles.cartItem__label}>Title</span>
          {isOnEditMode ? (
            <input
              value={title}
              onChange={event => setTitle(event.target.value)}
            />
          ) : (
            <h3 className={styles.cartItem__title} title={data.title}>
              {data.title}
            </h3>
          )}
        </div>

        {isOnEditMode ? (
          <GiButton
            title="Submit"
            variant="Primary"
            onClick={editCardItem}
            data-testid="card-submit-button"
          />
        ) : (
          <GiButton
            title="Edit"
            variant="Primary"
            onClick={toggleEditMode}
            data-testid="card-edit-button"
          />
        )}
      </div>
      <div className={styles.cartItem__body}>
        <span className={styles.cartItem__label}>Description</span>
        {isOnEditMode ? (
          <textarea
            value={description}
            className="d-block"
            onChange={event => setDescription(event.target.value)}
          />
        ) : (
          <h4 className={styles.cartItem__description} title={data.title}>
            {data.description}
          </h4>
        )}
      </div>
      <div className={styles.cartItem__footer}>
        <GiButton
          title="Delete"
          variant="Secondary"
          onClick={deleteCardItem}
          data-testid="card-delete-button"
        />
        <GiButton
          title="Chage List"
          variant="Primary"
          onClick={() => setIsDialogVisible(true)}
          data-testid="card-changeList-button"
        />
        {isDialogVisible && (
          <GiDialog
            onClose={() => {
              setIsDialogVisible(false);
            }}
            visible={isDialogVisible}
            content={<>{renderColumns()}</>}
            data-testid="changeList-dialog"
          />
        )}
      </div>
    </div>
  );
};

export default CardItem;
