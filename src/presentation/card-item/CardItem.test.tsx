import React from 'react';
import {fireEvent, screen} from '@testing-library/react';
import CardItem from './CardItem';
import {render} from '../../@core/testProviders';

const cardItemData = {
  id: 'id-1',
  title: 'Test Card',
  columnId: 'id-1',
  description: 'Test Description',
};

describe('Test CardItem Components', () => {
  it('should load the edit button', () => {
    render(<CardItem data={cardItemData} />);
    const buttonEl: HTMLElement = screen.getByTestId('card-edit-button');
    expect(buttonEl).toBeInTheDocument();
  });
  it('should load the delete button', () => {
    render(<CardItem data={cardItemData} />);
    const buttonEl: HTMLElement = screen.getByTestId('card-delete-button');
    expect(buttonEl).toBeInTheDocument();
  });
  it('should load the change list button', () => {
    render(<CardItem data={cardItemData} />);
    const buttonEl: HTMLElement = screen.getByTestId('card-changeList-button');
    expect(buttonEl).toBeInTheDocument();
  });
  it('should load dialod when click on the change list button', () => {
    render(<CardItem data={cardItemData} />);
    const buttonEl: HTMLElement = screen.getByTestId('card-changeList-button');
    fireEvent.click(buttonEl);
    const changeListDialogEl: HTMLElement =
      screen.getByTestId('changeList-dialog');
    expect(changeListDialogEl).toBeInTheDocument();
  });
});
