import React from 'react';
import {fireEvent, screen} from '@testing-library/react';
import ColumnsList from './ColumnsList';
import {render} from '../../@core/testProviders';

describe('Test ColumnsList Components', () => {
  it('should load the Add list button', () => {
    render(<ColumnsList />);
    const addListButtonEl: HTMLElement = screen.getByTestId('add-list-button');
    expect(addListButtonEl).toBeInTheDocument();
  });
  it('should add list by clicking on the Add list button', () => {
    render(<ColumnsList />);
    const addListButtonEl: HTMLElement = screen.getByTestId('add-list-button');
    fireEvent.click(addListButtonEl);
    const columnItemEl: HTMLElement = screen.getByTestId('column-item');
    expect(columnItemEl).toBeInTheDocument();
  });
});
