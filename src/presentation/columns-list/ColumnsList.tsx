import React, {useContext, useEffect} from 'react';
import styles from './ColumnsList.module.scss';
import {AppContext} from './../../contexts/app-context';
import ColumnItem from '../column-item/ColumnItem';
import {IColumnItemModel} from '../column-item/ColumnItem.models';
import {idGenerator} from '../../@core/utils';
import useLocalstorage from '../../hooks/useLocalstorage';
import {ICardItemModel} from '../card-item/CardItem.models';

const ColumnsList = () => {
  const {cards, columns, setCardsList, setColumnsList} = useContext(AppContext);
  const {setItem, getItem} = useLocalstorage();

  useEffect(() => {
    const columnsList = getItem<Array<IColumnItemModel>>('columnsList', true);
    const cardsList = getItem<Array<ICardItemModel>>('cardsList', true);
    if (cardsList && cardsList.length) {
      setCardsList(cardsList);
    }
    if (columnsList && columnsList.length) {
      setColumnsList(columnsList);
    }
  }, [getItem, setCardsList, setColumnsList]);

  useEffect(() => {
    setItem('cardsList', cards, true);
    setItem('columnsList', columns, true);
  }, [cards, columns, setItem]);

  const renderColumns = () => {
    return columns.map(item => <ColumnItem data={item} key={item.id} />);
  };

  const addColumnItem = () => {
    const newColumn: IColumnItemModel = {
      id: idGenerator(),
      title: 'New list',
    };
    setColumnsList([...columns, newColumn]);
  };

  return (
    <div className={styles.columnsContainer}>
      {renderColumns()}
      <div
        className={styles.addColumnButton}
        onClick={addColumnItem}
        data-testid="add-list-button"
      >
        Add a list
      </div>
    </div>
  );
};

export default ColumnsList;
