import React, {FC, useContext, useState} from 'react';
import styles from './AddCardForm.module.scss';
import {AppContext} from './../../contexts/app-context';
import {ICardItemModel} from './../card-item/CardItem.models';
import {idGenerator} from './../../@core/utils';

interface IAddCardFormProps {
  columnId: string;
}
const AddCardForm: FC<IAddCardFormProps> = ({columnId}) => {
  const [isActive, setIsActive] = useState<boolean>(false);
  const {cards, setCardsList} = useContext(AppContext);
  const addCard = (event: any) => {
    event.preventDefault();
    const {title, description} = event.target.elements;
    if (!title.value) {
      alert('Title is required');
      return;
    }
    const newCard: ICardItemModel = {
      id: idGenerator(),
      title: title.value,
      description: description.value,
      columnId: columnId,
    };
    setCardsList([...cards, newCard]);

    setIsActive(false);
  };
  return (
    <form className={styles.addCardForm} onSubmit={addCard}>
      <div className={styles.AddCardForm__body}>
        {isActive && (
          <div data-testid="add-card-inputs">
            <div className={styles.inputGroup}>
              <label className={styles.inputLabel}>Title*</label>
              <input className={styles.inputItem} type="text" name="title" />
            </div>
            <div className={styles.inputGroup}>
              <label className={styles.inputLabel}>Description</label>
              <textarea
                className={styles.inputItem}
                name="description"
              ></textarea>
            </div>
          </div>
        )}
      </div>
      <div className={styles.addCardForm__footer}>
        {isActive ? (
          <>
            <div className={styles.addCardForm__action}>
              <button
                type="submit"
                className={`${styles.addCardButton} ${styles.addCardButton__submit}`}
              >
                Add Card
              </button>
            </div>
            <div className={styles.addCardForm__action}>
              <div
                className={`${styles.addCardButton} ${styles.addCardButton__cancel}`}
                onClick={() => setIsActive(false)}
              >
                Cancel
              </div>
            </div>
          </>
        ) : (
          <div className={styles.addCardForm__action}>
            <div
              className={styles.addCardButton}
              onClick={() => setIsActive(true)}
              data-testid="add-card-button"
            >
              Add Card
            </div>
          </div>
        )}
      </div>
    </form>
  );
};

export default AddCardForm;
