import React, {FC, ReactElement} from 'react';
import {render, RenderOptions} from '@testing-library/react';
import {AppContextProvider} from './../contexts/app-context';

interface IpropsModel {
  children: ReactElement;
}

const AllTheProviders: FC<IpropsModel> = ({children}) => {
  return <AppContextProvider>{children}</AppContextProvider>;
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'wrapper'>,
) => render(ui, {wrapper: AllTheProviders, ...options});

export * from '@testing-library/react';
export {customRender as render};
