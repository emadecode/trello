import {idGenerator} from './utils';

describe('Test idGenerator from utils file', () => {
  it('should generate a random string', () => {
    const firstString = idGenerator();
    const secondString = idGenerator();
    expect(firstString).not.toEqual(secondString);
  });
});
