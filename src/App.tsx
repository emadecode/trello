import React, {useContext, useEffect} from 'react';
import './styles/global.scss';
import ColumnsList from './presentation/columns-list/ColumnsList';
import {AppContext, AppContextProvider} from './contexts/app-context';
import styles from './App.module.scss';
import useLocalstorage from './hooks/useLocalstorage';
import {ICardItemModel} from './presentation/card-item/CardItem.models';
import {IColumnItemModel} from './presentation/column-item/ColumnItem.models';

function App() {
  return (
    <div className={styles.App}>
      <AppContextProvider>
        <ColumnsList />
      </AppContextProvider>
    </div>
  );
}

export default App;
