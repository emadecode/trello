import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import App from './App';

describe('Test App Components', () => {
  it('should add multiple list on multiple click on the add list button', () => {
    render(<App />);
    const addListButtonEl: HTMLElement = screen.getByTestId('add-list-button');
    fireEvent.click(addListButtonEl);
    fireEvent.click(addListButtonEl);
    fireEvent.click(addListButtonEl);

    const columnsEl: Array<HTMLElement> = screen.getAllByTestId('column-item');
    expect(columnsEl.length).toBe(3);
  });

  it('should delete list when click on the delete button', () => {
    render(<App />);
    const addListButtonEl: HTMLElement = screen.getByTestId('add-list-button');
    fireEvent.click(addListButtonEl);
    fireEvent.click(addListButtonEl);
    let columnsEl: Array<HTMLElement> = screen.getAllByTestId('column-item');
    expect(columnsEl.length).toBe(5);
    const deleteListButtonEl: Array<HTMLElement> = screen.getAllByTestId(
      'column-delete-button',
    );
    fireEvent.click(deleteListButtonEl[0]);
    columnsEl = screen.getAllByTestId('column-item');
    expect(columnsEl.length).toBe(4);
  });
});
