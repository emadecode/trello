import React, {createContext, FC, useCallback, useState} from 'react';
import {ICardItemModel} from '../presentation/card-item/CardItem.models';
import {IColumnItemModel} from '../presentation/column-item/ColumnItem.models';

interface IAppContextModel {
  columns: Array<IColumnItemModel>;
  cards: Array<ICardItemModel>;
  setColumnsList: (columnsList: Array<IColumnItemModel>) => void;
  setCardsList: (cardsList: Array<ICardItemModel>) => void;
}

const AppContextInititalState: IAppContextModel = {
  columns: [],
  cards: [],
  setColumnsList: () => {},
  setCardsList: () => {},
};

export const AppContext = createContext<IAppContextModel>(
  AppContextInititalState,
);

interface IAppContextProvideProps {
  children: React.ReactElement;
}

export const AppContextProvider: FC<IAppContextProvideProps> = ({children}) => {
  const [appState, setAppState] = useState<IAppContextModel>(
    AppContextInititalState,
  );

  const setCardsList = useCallback((cardsList: Array<ICardItemModel>) => {
    setAppState(value => ({...value, cards: cardsList}));
  }, []);
  const setColumnsList = useCallback((columnsList: Array<IColumnItemModel>) => {
    setAppState(value => ({...value, columns: columnsList}));
  }, []);

  return (
    <AppContext.Provider value={{...appState, setCardsList, setColumnsList}}>
      {children}
    </AppContext.Provider>
  );
};
